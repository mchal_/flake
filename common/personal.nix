{pkgs, ...}: {
  users.users.michal = {
    description = "Michal S.";
    shell = pkgs.nushell;
    isNormalUser = true;
    extraGroups = ["wheel" "users" "audio" "video" "networkmanager" "podman" "docker"];
    uid = 1000;
  };

  security.sudo.extraConfig = "Defaults pwfeedback";
}
