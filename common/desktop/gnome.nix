{pkgs, ...}: {
  services.xserver = {
    enable = true;
    layout = "us";
    displayManager.gdm.enable = true;
    desktopManager.gnome = {
      enable = true;
      sessionPath = with pkgs; [libgtop gnome-menus libgda];
    };
  };

  services.udev.packages = with pkgs; [gnome.gnome-settings-daemon];

  environment.systemPackages = with pkgs; [
    gnomeExtensions.blur-my-shell
    gnomeExtensions.caffeine
    gnomeExtensions.containers
    gnomeExtensions.grand-theft-focus
    gnomeExtensions.gsconnect
    gnomeExtensions.rounded-window-corners
    gnomeExtensions.tiling-assistant
    gnomeExtensions.tophat
    gnomeExtensions.appindicator
    gnomeExtensions.dash-to-dock
    gnomeExtensions.pano
    gnomeExtensions.syncthing-indicator

    gnome.gnome-tweaks

    xorg.xkill
  ];

  programs.kdeconnect = {
    enable = true;
    package = pkgs.gnomeExtensions.gsconnect;
  };
}
