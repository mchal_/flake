{
  description = "Michal's NixOS/home-manager Flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    nixos-hardware.url = "github:nixos/nixos-hardware";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-on-droid = {
      url = "github:t184256/nix-on-droid";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-wsl = {
      url = "github:nix-community/nixos-wsl";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    nixpkgs,
    nixos-hardware,
    home-manager,
    nix-on-droid,
    nixos-wsl,
    ...
  } @ inputs: let
    modules = {
      home = {
        common = [./users/michal/shell.nix ./users/michal/base.nix];
        dev = [./users/michal/git.nix ./users/michal/gpg.nix];
      };
      nixos = {
        common = [
          ./common/nix.nix
          ./common/personal.nix
          ./common/yubikey.nix
        ];
        dev = [
          ./common/dev/distrobox.nix
          ./common/dev/podman.nix
          ./common/dev/qmk.nix
          ./common/dev/jetbrains.nix
        ];
        services = [
          ./devops/common.nix
          ./devops/adguard.nix
        ];
        desktops = {
          common = [
            ./common/desktop/media.nix
            ./common/desktop/gui-apps.nix
          ];
          kde =
            [
              ./common/desktop/kde.nix
            ]
            ++ modules.nixos.desktops.common;
          sway =
            [
              ./common/desktop/sway.nix
            ]
            ++ modules.nixos.desktops.common;
          gnome =
            [
              ./common/desktop/gnome.nix
            ]
            ++ modules.nixos.desktops.common;
        };
        server = [
          ./devops/common.nix
          ./devops/nextcloud.nix
          ./devops/lychee.nix
          ./devops/vaultwarden.nix
          ./devops/pronounce.nix
        ];
      };
    };

    pkgs = with nixpkgs.legacyPackages; {
      inherit x86_64-linux;
      inherit aarch64-linux;
    };

    defaultModules = x:
      nixpkgs.lib.forEach ["base.nix" "hardware.nix"] (
        mod: (./. + "/devices" + ("/" + x) + ("/" + mod))
      )
      ++ [./devices/common.nix]
      ++ [
        home-manager.nixosModules.home-manager
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.users.michal = {};
          home-manager.sharedModules = modules.home.common ++ modules.home.dev;
        }
      ];
  in {
    homeConfigurations = {
      "michal" = home-manager.lib.homeManagerConfiguration {
        pkgs = pkgs.x86_64-linux;
        modules =
          modules.home.common
          ++ modules.home.dev;
      };
      "michal@io" = home-manager.lib.homeManagerConfiguration {
        pkgs = pkgs.aarch64-linux;
        modules =
          modules.home.common
          ++ modules.home.dev;
      };
    };

    nixosConfigurations = {
      "ganymede" = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules =
          defaultModules "ganymede"
          ++ modules.nixos.desktops.gnome
          ++ modules.nixos.common
          ++ modules.nixos.dev
          ++ modules.nixos.services;
      };

      "europa" = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules =
          defaultModules "europa"
          ++ modules.nixos.desktops.gnome
          ++ modules.nixos.common
          ++ modules.nixos.dev
          ++ modules.nixos.services;
      };

      "io" = nixpkgs.lib.nixosSystem {
        system = "aarch64-linux";
        modules =
          defaultModules "io"
          ++ modules.nixos.common
          ++ modules.nixos.server
          ++ [nixos-hardware.nixosModules.raspberry-pi-4];
      };

      "juno" = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules =
          defaultModules "juno"
          ++ modules.nixos.desktops.gnome
          ++ modules.nixos.common
          ++ modules.nixos.dev
          ++ modules.nixos.services;
      };

      "wsl" = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules =
          [./common/personal.nix]
          ++ [
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.michal = {};
              home-manager.sharedModules = modules.home.common ++ modules.home.dev;
            }
          ]
          ++ [nixos-wsl.nixosModules.wsl];
      };
    };

    nixOnDroidConfigurations.default = nix-on-droid.lib.nixOnDroidConfiguration {
      modules = [
        ./devices/pixel/base.nix
      ];
    };

    devShells = {
      x86_64-linux.default = pkgs.x86_64-linux.mkShell {buildInputs = with pkgs.x86_64-linux; [statix];};
      aarch64-linux.default = pkgs.aarch64-linux.mkShell {buildInputs = with pkgs.aarch64-linux; [statix];};
    };

    formatter = {
      x86_64-linux = pkgs.x86_64-linux.alejandra;
      aarch64-linux = pkgs.aarch64-linux.alejandra;
    };
  };
}
