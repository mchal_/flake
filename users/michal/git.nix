{lib, ...}: {
  programs.git = {
    enable = true;
    userName = "Michal S.";
    userEmail = "mchal_@tuta.io";
    signing = {
      key = "C9CBDADE150C1E8B";
      signByDefault = true;
    };
    extraConfig = {
      safe.directory = "*";
      init.defaultBranch = "main";
    };
  };
}
