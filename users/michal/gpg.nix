{lib, ...}: {
  programs.gpg = {
    enable = true;
    scdaemonSettings = {
      card-timeout = "5";
      disable-ccid = true;
      reader-port = "Yubico Yubikey";
    };
    settings = {
      trust-model = "tofu+pgp";
      default-key = "C9CBDADE150C1E8B";
    };
  };

  services.gpg-agent = {
    enable = true;
    enableSshSupport = true;
    enableScDaemon = false;
    sshKeys = ["2E7B7CF23ACC452292F1C0745221D5D912086906"];
    pinentryFlavor = lib.mkDefault "curses";
  };
}
