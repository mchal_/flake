{
  pkgs,
  lib,
  ...
}: {
  home = {
    username = lib.mkDefault "michal";
    homeDirectory = lib.mkDefault "/home/michal";
    stateVersion = lib.mkDefault "23.05";
  };

  home.file.".face".source = ./var/pfp.png;
}
