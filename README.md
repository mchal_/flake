# Michal's Nix Flake

>>>
**⚠️ Warning**
 
I have documented my flake in this project's [Wiki](https://gitlab.com/mchal_/flake/-/wikis/Prefaces), I have no idea why I've done this. But give it a read if you're interested !
>>>


Idk why you'd be reading this, but hi !

It's likely that I maybe linked you to take a look at this flake as a reference for a Nix-related concept.

It's also possible you just stumbled here by accident! Or that you were browsing my profile.

### Feel free to:
- Fork this
- Copy any ideas, concepts, or formatting
- Donate all of your blood to me
- Frame me for a crime (As long as it's something cool, like running over your rival in a steamroller)
- Leave me an angry email about any topic. I read all of my emails and I think that'd be pretty funny

If you have any questions, feel free to either message me on Matrix at @michal:getcryst.al,
Or if you're feeling particularly formal and/or antequated, you can e-mail me at mchal_@tuta.io
I'm also reachable by summoning circle, although you'd have to figure out the incantation for that yourself.

Au revoir ! 
