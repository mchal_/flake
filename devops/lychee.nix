let
  domain = "lychee.devops.gay";
  port = 8080;
in {
  virtualisation.oci-containers.containers = {
    "lychee" = {
      image = "lycheeorg/lychee:v4.6.1";
      ports = ["${toString port}:80"];
      autoStart = true;
      volumes = [
        "/var/www/lychee/conf:/conf"
        "/var/www/lychee/uploads:/uploads"
        "/var/www/lychee/sym:/sym"
      ];
    };
  };

  systemd.tmpfiles.rules = [
    "d /var/www/lychee/conf 0777 root root"
    "d /var/www/lychee/uploads 2777 root root"
    "d /var/www/lychee/sym 2777 root root"
  ];

  services.nginx.virtualHosts."lychee.devops.gay" = {
    enableACME = true;
    forceSSL = true;
    locations."/" = {
      recommendedProxySettings = true;
      proxyPass = "http://127.0.0.1:${toString port}";
    };
  };
}
