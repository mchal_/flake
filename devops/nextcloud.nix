let
  domain = "devops.gay";
  adminuser = "michal";
in {
  services.nextcloud = {
    enable = true;
    hostName = domain;
    config = {
      dbtype = "pgsql";
      dbuser = "nextcloud";
      dbhost = "/run/postgresql";
      dbname = "nextcloud";
      overwriteProtocol = "https";
      adminpassFile = "/secret";
      inherit adminuser;
    };
    autoUpdateApps = {
      enable = true;
      startAt = "04:00:00";
    };
    https = true;
    webfinger = true;
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = ["nextcloud"];
    ensureUsers = [
      {
        name = "nextcloud";
        ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
      }
    ];
  };

  systemd.services."nextcloud-setup" = {
    requires = ["postgresql.service"];
    after = ["postgresql.service"];
  };

  services.nginx.virtualHosts."${domain}" = {
    enableACME = true;
    forceSSL = true;
  };
}
