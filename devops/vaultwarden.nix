let
  domain = "vault.devops.gay";
  port = 8100;
in {
  services.vaultwarden = {
    enable = true;
    backupDir = "/var/www/vaultwarden/backups";
    dbBackend = "sqlite";
    config = {
      DOMAIN = "https://${domain}";
      SIGNUPS_ALLOWED = false;

      ROCKET_ADDRESS = "127.0.0.1";
      ROCKET_PORT = "${toString port}";
      ROCKET_LOG = "critical";
    };
  };

  systemd.tmpfiles.rules = [
    "d /var/www/vaultwarden/backups 0755 vaultwarden vaultwarden"
  ];

  services.nginx.virtualHosts."${domain}" = {
    enableACME = true;
    forceSSL = true;
    locations."/" = {
      proxyPass = "http://127.0.0.1:${toString port}";
    };
  };
}
