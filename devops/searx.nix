{lib, ...}: {
  services.searx = {
    enable = true;
    settings = {
      server = {
        port = 8081;
        bind_address = "127.0.0.1";
        base_url = "http://a.out:8081";
        secret_key = "b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9";
        image_proxy = true;
        default_theme = "oscar";
      };
      general.instance_name = "Personal SearX";
    };
  };

  networking.hosts = {
    "127.0.0.1" = ["searx.local"];
  };
}
