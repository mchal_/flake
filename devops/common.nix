{
  networking = {
    firewall.allowedTCPPorts = [
      443
      80
    ];
    hosts = {
      "127.0.0.1" = ["a.out"];
    };
  };

  security.acme = {
    defaults.email = "mchal_@tuta.io";
    acceptTerms = true;
  };
}
