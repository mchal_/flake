{pkgs, ...}: let
  port = 8802;
in {
  services.trilium-server = {
    enable = true;
    inherit port;
    instanceName = "devops.gay Trilium";
  };

  services.nginx.virtualHosts."trilium.devops.gay" = {
    enableACME = true;
    forceSSL = true;
    locations."/" = {
      proxyPass = "http://0.0.0.0:${toString port}";
    };
  };
}
