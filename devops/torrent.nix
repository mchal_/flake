let
  transmissionPort = 9091;
  trackerPort = 6969;
in {
  services = {
    opentracker = {
      enable = true;
      extraOptions = ''
        listen.tcp_udp 0.0.0.0:${toString trackerPort}
      '';
    };
    transmission = {
      enable = true;
      openRPCPort = true;
      openPeerPorts = true;
      credentialsFile = "/transmission.json";
      settings.rpc-port = transmissionPort;
    };
    nginx.virtualHosts."transmission.devops.gay" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString transmissionPort}";
      };
    };
  };

  networking.firewall = {
    allowedTCPPorts = [
      trackerPort
    ];
    allowedUDPPorts = [
      trackerPort
    ];
  };
}
