{
  services.adguardhome = {
    enable = true;
    mutableSettings = true;
    settings = {
      dns = {
        bind_hosts = ["127.0.0.1"];
        bootstrap_dns = ["1.1.1.1" "8.8.8.8" "8.8.4.4"];
      };
    };
  };

  networking.hosts = {
    "127.0.0.1" = ["adguard.local"];
  };
}
