{pkgs, ...}: let
  port = 8801;
in {
  services.tandoor-recipes = {
    enable = true;
    inherit port;
    extraConfig = {
      ALLOWED_HOSTS = "tandoor.devops.gay";
    };
  };

  services.nginx.virtualHosts."tandoor.devops.gay" = {
    enableACME = true;
    forceSSL = true;
    locations."/" = {
      recommendedProxySettings = true;
      proxyPass = "http://0.0.0.0:${toString port}";
    };
  };
}
