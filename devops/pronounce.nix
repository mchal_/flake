{pkgs, ...}: let
  version = "v2.2.1";
  port = 8800;
  inherit (pkgs.python39Packages) poetry;
in {
  systemd = {
    services.pronounce = {
      enable = true;
      after = ["network-online.target"];
      wantedBy = ["multi-user.target"];
      script = ''
        [ -d "/var/www/pronounce" ] && rm -rf /var/www/pronounce

        ${pkgs.git}/bin/git clone https://gitlab.com/mchal_/pronounce /var/www/pronounce --depth 1 --branch ${version}

        cd /var/www/pronounce
        ${poetry.out}/bin/poetry install
        ${poetry.out}/bin/poetry run gunicorn pronounce.app:app -b 0.0.0.0:${toString port}
      '';
    };
    tmpfiles.rules = [
      "d /var/www/pronounce 0755 root root"
    ];
  };

  services.nginx.virtualHosts."me.devops.gay" = {
    enableACME = true;
    forceSSL = true;
    locations."/" = {
      recommendedProxySettings = true;
      proxyPass = "http://0.0.0.0:${toString port}";
    };
  };
}
