{pkgs, ...}: {
  boot.kernelPackages = pkgs.linuxPackages_latest;

  networking = {
    hostName = "juno";
    nameservers = ["127.0.0.1"];
    interfaces = {
      eno1.useDHCP = true;
      wlp2s0.useDHCP = true;
    };
  };

  services = {
    xserver.libinput.enable = true;
    auto-cpufreq.enable = true;
  };

  system.stateVersion = "22.11";
}
