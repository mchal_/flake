{pkgs, ...}: {
  boot.kernelPackages = pkgs.linuxPackages_latest;

  networking = {
    hostName = "europa";
    nameservers = ["127.0.0.1"];
    interfaces = {
      enp3s0.useDHCP = true;
      wlp2s0.useDHCP = true;
    };
  };

  services = {
    xserver.libinput.enable = true;
    auto-cpufreq.enable = true;
  };

  system.stateVersion = "22.11";
}
