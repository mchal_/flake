{lib, ...}: {
  boot = {
    cleanTmpDir = true;
    loader = {
      systemd-boot = {
        enable = true;
      };
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot/efi";
      };
    };
  };

  networking = {
    networkmanager.enable = true;
    useDHCP = false;
  };

  time.timeZone = "Europe/London";

  fileSystems = {
    "/" = lib.mkDefault {
      device = "/dev/disk/by-label/ROOT";
      fsType = "btrfs";
      options = ["compress=zstd" "noatime"];
    };
    "/boot/efi" = {
      device = "/dev/disk/by-label/EFI";
      fsType = "vfat";
    };
  };

  boot.plymouth = {
    enable = true;
    theme = "bgrt";
  };

  zramSwap.enable = true;

  swapDevices = [{device = "/dev/disk/by-label/SWAP";}];
}
