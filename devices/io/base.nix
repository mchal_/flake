{pkgs, ...}: {
  boot.loader.generic-extlinux-compatible.enable = false;

  networking = {
    hostName = "io";
    interfaces = {
      eth0.useDHCP = true;
      wlan0.useDHCP = true;
    };
  };

  services.openssh = {
    enable = true;
    ports = [2202];
  };

  environment.systemPackages = with pkgs; [libraspberrypi];

  system.stateVersion = "22.11";
}
