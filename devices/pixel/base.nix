{pkgs, ...}: {
  imports = [
    ./ssh.nix
  ];

  home-manager.config = {pkgs, ...}: {
    imports = [
      ../../users/michal/base.nix
      ../../users/michal/shell.nix
      ../../users/michal/dev.nix
    ];
    services.syncthing.enable = false;
    manual.manpages.enable = false;

    programs.git.extraConfig.core.editor = "vim";
    services.gpg-agent.pinentryFlavor = "curses";

    home.stateVersion = "22.05";
  };

  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  system.stateVersion = "22.05";
}
