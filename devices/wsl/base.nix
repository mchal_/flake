{pkgs, ...}: {
  networking.hostName = "wsl";
  wsl.defaultUser = "michal";
  system.stateVersion = "23.05";
}
