{pkgs, ...}: {
  boot.kernelPackages = pkgs.linuxPackages_latest;

  networking = {
    hostName = "ganymede";
    nameservers = ["127.0.0.1"];
    interfaces = {
      eno1.useDHCP = true;
      wlp4s0.useDHCP = true;
    };
  };

  services.openssh = {
    enable = true;
    ports = [22];
  };

  services.xserver.videoDrivers = ["nvidia"];
  hardware.nvidia.modesetting.enable = true;

  system.stateVersion = "22.11";
}
